# Partie 1

## Sous-partie 1 : texte

une phrase sans rien

*une phrase en italique*

__une phrase en gras__

Un lien vers [fun-mooc.fr](https://lms.fun-mooc.fr/)

Une ligne de 'code'

## Sous-partie 2 : listes

**Liste à puce**

*item

    *sous-item

    *sous-item

*item

*item

**Liste numérotée**

1.item

2.item

3.item

## Sous-partie 3 : code

'''
    # Extrait de code 
'''


